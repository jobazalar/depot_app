class Product < ActiveRecord::Base
  # Atributos
  attr_accessible :title, :description, :image_url, :price
  # Valida que title, description y image_url no esten en blanco
  validates :title, :description, :image_url, presence: true
  # Valida que el precio se mayor o igual a 0.01
  validates :price, numericality: {greater_than_or_equal_to: 0.01}
  # Valida que el titulo de cada producto sea unico
  validates :title, uniqueness: true
  # Valida la url de la imagen
  validates :image_url, allow_blank: true, format: {
    with:    %r{\.(gif|jpg|png)$}i,
    message: 'must be a URL for GIF, JPG or PNG image.'
  }
end
